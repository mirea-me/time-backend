'use strict'

const { test, trait } = use('Test/Suite')('Authorization')
const Env = use('Env')
const User = use('App/Models/User')
const Token = use('App/Models/Token')
const Event = use('App/Models/Event')

trait('Test/ApiClient')

const TESTUSER_LOGIN = Env.get('TESTUSER_LOGIN')
const TESTUSER_PASSWORD = Env.get('TESTUSER_PASSWORD')

test('Очистка возможного мусора от старых тестов', async() => {
    const testuser = await User.findBy('username', TESTUSER_LOGIN)
    if (testuser) {
        await Event
            .query()
            .where('creator_id', testuser.id)
            .delete()
        await Token
            .query()
            .where('user_id', testuser.id)
            .delete()
        await testuser.delete()
    }
})

test('Регистрация тестового юзера', async({ client }) => {
    const response = await client
        .post('/auth/register')
        .send({
            username: TESTUSER_LOGIN,
            realname: 'Иванов Иван Иванович',
            email: 'testuser@mirea.me',
            password: TESTUSER_PASSWORD,
            password_confirmation: TESTUSER_PASSWORD
        })
        .end()

    response.assertStatus(200)
    response.assertText('true')
})

test('Авторизация через логин и пароль', async({ client }) => {
    const response = await client
        .post('/auth/login')
        .send({
            username: TESTUSER_LOGIN,
            password: TESTUSER_PASSWORD,
        })
        .end()

    response.assertStatus(200)
    response.assertText('true')
})