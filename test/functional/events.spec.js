'use strict'

const { test, trait } = use('Test/Suite')('Events')
//const Env = use('Env')

trait('Test/ApiClient')

//const TESTUSER_LOGIN = Env.get('TESTUSER_LOGIN')

test('make sure 2 + 2 is 4', async ({ assert }) => {
    assert.equal(2 + 2, 4)
})
