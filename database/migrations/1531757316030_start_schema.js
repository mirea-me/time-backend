'use strict'

const Schema = use('Schema')

class StartSchema extends Schema {
    up() {
        this.create('users', (table) => {
            table.increments()
            table.string('username', 60).notNullable().unique()
            table.string('email', 60).notNullable().unique()
            table.string('password', 60).notNullable()
            table.string('realname', 60).notNullable()
            table.integer('role').notNullable().unsigned().defaultTo(0)
            table.json('tags').notNullable().defaultTo('[]')
            table.timestamps()
        })
        this.create('tokens', (table) => {
            table.increments()
            table.integer('user_id').unsigned().references('id').inTable('users')
            table.string('token', 255).notNullable().unique().index()
            table.string('type', 80).notNullable()
            table.boolean('is_revoked').defaultTo(false)
            table.timestamps()
        })
        this.create('events', (table) => {
            table.increments()
            table.integer('creator_id').unsigned().references('id').inTable('users')
            table.string('title', 255).notNullable()
            table.text('description')
            table.json('tags').notNullable().defaultTo('[]')
            table.timestamp('time_start').notNullable()
            table.timestamp('time_end').notNullable()
            table.timestamps()
        })
        this.create('tags', (table) => {
            table.increments()
            table.string('name', 255).notNullable().unique().index()
            table.boolean('private').defaultTo(false)
            table.timestamps()
        })
    }

    down() {
        this.drop('tokens')
        this.drop('users')
        this.drop('events')
        this.drop('tags')
    }
}

module.exports = StartSchema