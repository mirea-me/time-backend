'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

const Route = use('Route')

Route
    .group(() => {
        Route.post('login', 'AuthController.login')
        Route.post('register', 'AuthController.register')
        Route.get('logout', 'AuthController.logout')
    })
    .prefix('auth')

Route
    .group(() => {
        Route.get('timerange', 'EventsController.timerange')
        Route.post('get', 'EventsController.get')
    })
    .prefix('events')
    .middleware(['auth'])