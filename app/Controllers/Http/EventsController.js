'use strict'

const Event = use('App/Models/Event')
const { validateAll } = use('Validator')

class EventsController {
    async get({ request, response }) {
        const validation = await validateAll(request.all(), {
            time_start: 'required|date',
            time_end: 'required|date'
        }, {
            'time_start.required': 'Не указан фильтр начальной даты',
            'time_start.date': 'Переданный фильтр начальной даты не является понятным системе',

            'time_end.required': 'Не указан фильтр конечной даты',
            'time_end.date': 'Переданный фильтр конечной даты не является понятным системе'
        })

        if (validation.fails()) {
            return response.status(400).send(validation.messages())
        }

        const { time_start, time_end } = request.all()

        const events = await Event
            .query()
            .where('time_start', '>', time_start)
            .where('time_end', '<', time_end)
            .fetch()

        return events
    }

    async timerange() {
        const [firstEntries, lastEntries] = await Promise.all([Event.pick(), Event.pickInverse()])
        const [jsonFirst, jsonLast] = [firstEntries.toJSON()[0], lastEntries.toJSON()[0]]

        return {
            start: jsonFirst.time_start,
            end: jsonLast.time_end
        }
    }
}

module.exports = EventsController