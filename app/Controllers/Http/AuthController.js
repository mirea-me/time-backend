'use strict'

const User = use('App/Models/User')
const { rule, validateAll } = use('Validator')

class AuthController {
    async login({ auth, request, response }) {
        const validation = await validateAll(request.all(), {
            username: 'required|alpha_numeric|min:5|max:30',
            password: 'required|string|min:5|max:30'
        }, {
            'username.required': 'Вы не указали имя пользователя',
            'username.alpha_numeric': 'Имя пользователя должно включать в себя только латиницу и цифры',
            'username.min': 'Имя пользователя не может быть длиной меньше 5 символов',
            'username.max': 'Имя пользователя не может быть длиной больше 30 символов',

            'password.required': 'Вы не указали пароль учетной записи',
            'password.string': 'Поле пароля должно быть передано строкой',
            'password.min': 'Пароль не может быть длиной меньше 5 символов',
            'password.max': 'Пароль не может быть длиной больше 30 символов'
        })

        if (validation.fails()) {
            return response.status(400).send(validation.messages())
        }

        const { username, password } = request.all()

        try {
            await auth.remember(true).attempt(username, password)
            return true
        } catch (error) {
            if (error.name === 'UserNotFoundException') {
                return response.status(400).send([{
                    message: 'Пользователь с данным логином не найден'
                }])
            }
            if (error.name === 'PasswordMisMatchException') {
                return response.status(400).send([{
                    message: 'Введенный вами пароль неверен'
                }])
            }
            if (error.name === 'RuntimeException' && error.code === 'E_CANNOT_LOGIN') {
                return response.status(400).send([{
                    message: 'Не удалось завершить процесс авторизации, возможно вы уже авторизованы?'
                }])
            }

            console.error(error)
            return response.status(500).send([{
                message: 'Получена неизвестная ошибка, данные записаны и переданы администрации'
            }])
        }
    }

    async register({ auth, request, response }) {
        const validation = await validateAll(request.all(), {
            username: 'required|alpha_numeric|min:5|max:30|unique:users,username',
            email: 'required|email|max:30|unique:users,email',
            password: 'required|string|min:5|max:30|confirmed',
            realname: [
                rule('required'),
                rule('regex', /^([А-Я][а-я]+)( [А-Я][а-я]+)( [А-Я][а-я]+)?$/)
            ]
        }, {
            'username.required': 'Вы не указали имя пользователя',
            'username.alpha_numeric': 'Имя пользователя должно включать в себя только латиницу и цифры',
            'username.min': 'Имя пользователя не может быть длиной меньше 5 символов',
            'username.max': 'Имя пользователя не может быть длиной больше 30 символов',
            'username.unique': 'Данное имя пользователя уже кто-то использует',

            'password.required': 'Вы не указали пароль учетной записи',
            'password.string': 'Поле пароля должно быть передано строкой',
            'password.min': 'Пароль не может быть длиной меньше 5 символов',
            'password.max': 'Пароль не может быть длиной больше 30 символов',
            'password.confirmed': 'Введенный подтверждающий пароль не соответствует изначальному',

            'email.required': 'Вы не указали email',
            'email.email': 'Введенный email не соответствует формату. Пример: admin@mirea.me',
            'email.max': 'Email не может быть длиной больше 30 символов',
            'email.unique': 'Данный email уже кто-то использует',

            'realname.required': 'Вы не указали ФИО',
            'realname.regex': 'Введенное ФИО не соответствует формату. Пример: Иванов Иван Иванович'
        })

        if (validation.fails()) {
            return response.status(400).send(validation.messages())
        }

        try {
            const user = await User.create(
                request.only(['username', 'email', 'password', 'realname'])
            )
            await auth.remember(true).login(user)
            return true
        } catch (error) {
            console.error(error)
            return response.status(500).send([{
                message: 'Получена неизвестная ошибка, данные записаны и переданы администрации'
            }])
        }
    }

    async logout({ auth, response }) {
        try {
            await auth.logout()
            return true
        } catch (error) {
            return response.status(401).send([{
                message: 'Вы не авторизованы'
            }])
        }
    }
}

module.exports = AuthController
