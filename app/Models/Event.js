'use strict'

const Model = use('Model')

class Event extends Model {
    static get visible() {
        return ['id', 'title', 'description', 'time_start', 'time_end']
    }

    static get hidden() {
        return ['creator_id', 'created_at', 'updated_at']
    }
}

module.exports = Event